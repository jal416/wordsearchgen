/*
 James Lamberti
 Program Description: Program creates word search puzzles
 */

public class Prog4 {

    public static void main(String[] args) {
        int order = 0;
        String fileName = null;
        if (args.length < 2) {
            System.out.println("Usage: wordsearch <order> <wordfilename>");
            return;
        } else {
            try {
                order = Integer.parseInt(args[0]);
            } catch (Exception ex) {
                System.out.println("Invalid order: " + args[0]);
                return;
            }
            fileName = args[1];
        }
        Puzzle p = new Puzzle(order, fileName);
        if (p.createPuzzle()) {
            p.display();
            p.fillRandom();
            p.display();
        } else {
            System.out.println("Can't create puzzle.");
        }
    }
}
