/*
 James Lamberti
 Program Description: Stores the list of words in the word search
 */

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author Jack
 */
public class WordList implements Comparator<String> {

    private Word[] words;
    private String EOL = System.lineSeparator();
    private String fileName;

    public WordList(String fileName) {
        this.fileName = fileName;
    }

    public boolean readFile() {
        File f = new File(fileName);
        if (f.exists()) {   // If file exists, read file
            FileInputStream fistream = null;
            try {
                fistream = new FileInputStream(fileName);
                try (DataInputStream in = new DataInputStream(fistream)) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    String line;
                    LineNumberReader lnr = new LineNumberReader(new FileReader(f));
                    lnr.skip(Long.MAX_VALUE);   //Skip to the end of the file
                    words = new Word[lnr.getLineNumber() + 1];
                    String[] strWords = new String[lnr.getLineNumber() + 1];
                    int counter = 0;
                    while ((line = br.readLine()) != null) {
                        if (!line.equals("")) {
                            strWords[counter] = line.trim();
                            counter++;
                        }
                    }
                    Collections.sort(Arrays.asList(strWords), this);    //Sort the array for extra credit
                    for (int i = 0; i < strWords.length; i++) {
                        words[i] = new Word(strWords[i]);
                    }
                }
            } catch (IOException ex) {
                System.out.println("Error when reading file..." + EOL + "Program will now exit");
                return false;
            } finally {
                try {
                    fistream.close();
                } catch (IOException ex) {  //Just in case...
                    System.out.println("Error when reading file..." + EOL + "Program will now exit");
                    return false;
                }
            }
        } else {    //File doesn't exist...
            System.out.println("File does not exist!" + EOL + "Program will now exit");
            return false;
        }
        return true;
    }

    public Word[] getList() {
        return words;
    }

    @Override
    public int compare(String o1, String o2) {
        /*
         * The Array of words should by length because
         * the more difficult words to place (long ones)
         * should be placed first, then the smaller ones
         * can be filled in around them
         */
        if (o1.length() < o2.length()) {
            return 1;
        } else if (o1.length() > o2.length()) {
            return -1;
        }
        return o1.compareTo(o2);
    }
}
