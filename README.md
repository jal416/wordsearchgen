# Word Search Generator #
This program takes a file containing words and produces a word search from it
## How to run ##

```
#!bash
javac *.java
java WordSearchGen 40 states.txt

```
Which produces:
![answer.png](https://bitbucket.org/repo/ApB5Rq/images/879451455-answer.png)

![puzzle.png](https://bitbucket.org/repo/ApB5Rq/images/1024945768-puzzle.png)