/*
 James Lamberti
 Program Description: holds a word that will go into the cross word puzzle
 */

public class Word {

    private String wordStr;
    private int col;
    private int row;
    private int direction;

    public Word(String wordStr) {
        this.wordStr = wordStr.toUpperCase();
        col = 0;
        row = 0;
    }

    public String getWordStr() {
        return wordStr;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public int getDirection() {
        return direction;
    }

    public void setWordStr(String wordStr) {
        this.wordStr = wordStr.toUpperCase();
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
}
