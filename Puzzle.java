/*
 James Lamberti
 Program Description: Creates the word search puzzles
*/

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Jack
 */
public class Puzzle {

    private char[][] wordSearch;
    private String fileName;
    private int order;
    private final char BLANK = '*';

    public Puzzle(int order, String fileName) {
        this.order = order;
        this.fileName = fileName;
        wordSearch = new char[order][order];
        for (int i = 0; i < order; i++) {
            for (int j = 0; j < order; j++) {
                wordSearch[i][j] = BLANK;
            }
        }
    }

    private int pickRow() {
        Random rand = new Random();
        return rand.nextInt(order);
    }

    private int pickRow(int uBound) {
        Random rand = new Random();
        return rand.nextInt(uBound);
    }

    private int pickCol() {
        Random rand = new Random();
        return rand.nextInt(order);
    }

    private int pickCol(int uBound) {
        Random rand = new Random();
        return rand.nextInt(uBound);
    }

    private boolean check(Word word, int row, int col, int direction) {
        String strWord = word.getWordStr();
        int[][] directionsArr = {{0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}};
        if ((((0 > (row + (directionsArr[direction][0] * strWord.length()))) || (0 > (col + (directionsArr[direction][1] * strWord.length()))) || (order <= (row + (directionsArr[direction][0] * strWord.length()))) || (order <= (col + (directionsArr[direction][1] * strWord.length())))))) {
            return false;
        }

        for (int i = 0; i < strWord.length(); i++) {
            if (!((wordSearch[row + (i * directionsArr[direction][0])][col + (i * directionsArr[direction][1])] == strWord.charAt((i))) || (wordSearch[row + (i * directionsArr[direction][0])][col + (i * directionsArr[direction][1])] == BLANK))) {
                return false;
            }
        }
        return true;
    }

    private void place(Word word, int row, int col, int direction) {
        int[][] directions = {{0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}};
        String strWord = word.getWordStr();
        word.setCol(col);
        word.setRow(row);
        word.setDirection(direction);
        for (int i = 0; i < strWord.length(); i++) {
            wordSearch[row][col] = strWord.charAt(i);
            row += directions[direction][0];
            col += directions[direction][1];
        }
    }

    private boolean attemptPlaceWord(Word word) {
        if (word.getWordStr().length() > order) {
            //Don't even bother testing...it won't fit
            return false;
        }
        ArrayList<Integer> possibleRows = new ArrayList<>();
        ArrayList<Integer> possibleCols = new ArrayList<>();
        for (int i = 0; i < order; i++) {
            for (int j = 0; j < order; j++) {
                possibleRows.add(i);
                possibleCols.add(i);
            }
        }
        while (!(possibleRows.isEmpty())) {
            int testRow = pickRow(possibleRows.size());
            int testCol = pickCol(possibleCols.size());
            for (int i = 0; i < 8; i++) {
                if (check(word, possibleRows.get(testRow), possibleCols.get(testCol), i)) {
                    place(word, possibleRows.get(testRow), possibleCols.get(testCol), i);
                    return true;
                }
            }
            //didn't work for these spots so remove them
            possibleRows.remove(testRow);
            possibleCols.remove(testCol);
        }

        return false;
    }

    public void fillRandom() {
        Random rand = new Random();
        for (int i = 0; i < order; i++) {
            for (int j = 0; j < order; j++) {
                if (wordSearch[i][j] == BLANK) {
                    wordSearch[i][j] = (char) (rand.nextInt(26) + 'A');
                }
            }
        }
    }

    public void display() {
        for (int i = 0; i < order; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < order; j++) {
                sb.append(wordSearch[i][j]);
                sb.append(" ");
            }
            System.out.println(sb.toString());
        }
        System.out.println("");
    }

    public boolean createPuzzle() {
        WordList wordList = new WordList(this.fileName);
        if (wordList.readFile()) {
            for (Word word : wordList.getList()) {
                if (!(attemptPlaceWord(word))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}
